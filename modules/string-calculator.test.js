const add = require('./string-calculator');

test('Empty string to equal 0', () => {
  expect(add('')).toBe(0);
});

test('a number to equal same number', () => {
  expect(add('4')).toBe(4);
});

test('adds 1 + 2 to equal 3', () => {
  expect(add('1,2')).toBe(3);
});

test('handle unknown amount of numbers', () => {
  const quantity = Math.floor(Math.random() * 100);
  const arr = Array.from({length: quantity}, () => Math.floor(Math.random() * 100));
  const sum = arr.reduce((a, b) => a + b);

  const result = add(arr.toString());

  expect(result).toBe(sum);
});

test('handle new lines \\n between numbers', () => {
  const numbers = '1\n2,3';

  const result = add(numbers);

  expect(result).toBe(6);
});

test('support different delimiter', () => {
  const s = '//;\n1;2;3';

  const result = add(s);
  
  expect(result).toBe(6);
});

test('negative values not allowed', () => {
  const s = '//;\n1;3;78;-3;2;6;-24';
  let error = '';

  try {
    add(s);
  } catch(err) {
    error = err.toString();
  }

  expect(error).toMatch(/negatives not allowed -3,-24/);
});

test('do not allow values >1000', () => {
  const s = '3,1000,1001';

  const result = add(s);

  expect(result).toBe(1003);
});

test('delimiters can be of any length', () => {
  const s = '//[**]\n1**2**3';

  const result = add(s);

  expect(result).toBe(6);
});

test('allow multiple single char delimiters', () => {
  const s = '//[#][&]\n1#2&3';

  const result = add(s);

  expect(result).toBe(6);
});

test('allow multiple delimiters of any length', () => {
  const s = '//[##][^^^][;;]\n1##2^^^3;;4';

  const result = add(s);

  expect(result).toBe(10);
})