function getDelimiter(s) {
  if (s.startsWith('//')) {
    var regex1 = /(?<=\[).+?(?=\])/g;
    var matches = s.match(regex1);

    if (matches === null) {
      return s.substring(2, s.indexOf('\n'));
    }
    return matches;
  }
  return ',';
}

function sanitize(s, delimiter = ',') {
  if (s.startsWith('//')) {
    return s.slice(s.indexOf('\n') + 1).replace(/\n/g, delimiter);
  }
  return s.replace(/\n/g, delimiter);
}

function reducer(accumulator, currentValue) {
  if (currentValue > 1000) {
    return Number(accumulator);
  }
  return Number(accumulator) + Number(currentValue);
}

function add(s) {
  const delimiter = getDelimiter(s);
  let sanitizedString = sanitize(s, delimiter);
  let valuesArray = [];
  
  if (!Array.isArray(delimiter)) {
    valuesArray = sanitizedString.split(delimiter);
  } else {
    delimiter.forEach(token => {
      sanitizedString = sanitizedString.split(token).join();
    });
    valuesArray = sanitizedString.split(',');
  }

  const negatives = valuesArray.filter(val => Number(val) < 0);
  if (negatives.length > 0) {
    throw Error('negatives not allowed ' + negatives.join());
  }

  return Number(valuesArray.reduce(reducer));
};

module.exports = add;
