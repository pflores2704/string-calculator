# String Calculator
A simple string calculator

### Installation

The String Calculator requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and run the tests.

```sh
$ cd string-calculator
$ npm install
$ npm run test
```

Or, if Node.js is not installed locally you can alternatively use the docker image by updating the .env file with your current path and spinning up the container:

```sh
$ docker-compose up -d
```
Finally, you can run the test suite in the container by executing the following command:
```sh
$ docker exec -it node-app npm run test
```